import { Button, Col, Row } from "react-bootstrap";

export default function Banner({ isError }) {
    if (isError) {
      return (
        <Row>
          <Col className='p-5'>
            <h1>Page Not Found</h1>
            <p>Go back to the <a href="/">homepage</a></p>
          </Col>
        </Row>
      );
      
    }

    return (
      <Row>
        <Col className='p-5'>
          <h1>Zuitt Coding Bootcamp</h1>
          <p>Opportunities for everyone</p>
          <Button variant='primary'>Enroll Now!</Button>
        </Col>
      </Row>
    );
  }

/*
- JSX syntax requires that components should always have closing tags.

- In the example above, since the "AppNavbar" component does not require any text to be placed in between, we add a "/" at the end to signify that they are self-closing tags. 
      
- The "className" prop is used in place of the "class" attribute for HTML tags in React JS due to our use of JSX elements.


        - It's also good practice to organize importing of modules to improve code readability.
    - The example above follows the following pattern:
        - imports from built-in react modules
        - imports from downloaded packages
        - imports from user defined components
*/