import {Row, Col, Card} from 'react-bootstrap'

export default function Highlights(){
    return (
        <Row className='mt-3'>
            <Col xs={12} md={4}>
                <Card className='cardHighlight p-3'>
                    <Card.Body>
                        <Card.Title>
                            <h2>Learn from Home</h2>
                        </Card.Title>
                        <Card.Text>
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Illum praesentium minus voluptates nihil tempore. Sint vel voluptatem tempore laboriosam error debitis rerum quo harum in ea, sequi adipisci quas possimus neque incidunt cum cupiditate, perspiciatis dignissimos blanditiis quasi eius? Amet sed numquam ratione totam aliquid nihil? Deleniti laboriosam accusantium placeat voluptate 
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>

            <Col xs={12} md={4}>
                <Card className='cardHighlight p-3'>
                    <Card.Body>
                        <Card.Title>
                            <h2>Study now, Pay Later</h2>
                        </Card.Title>
                        <Card.Text>
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Illum praesentium minus voluptates nihil tempore. Sint vel voluptatem tempore laboriosam error debitis rerum quo harum in ea, sequi adipisci quas possimus neque incidunt cum cupiditate, perspiciatis dignissimos blanditiis quasi eius? Amet sed numquam ratione totam aliquid nihil? Deleniti laboriosam accusantium placeat voluptate 
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>

            <Col xs={12} md={4}>
                <Card className='cardHighlight p-3'>
                    <Card.Body>
                        <Card.Title>
                            <h2>Be a Part of our Community</h2>
                        </Card.Title>
                        <Card.Text>
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Illum praesentium minus voluptates nihil tempore. Sint vel voluptatem tempore laboriosam error debitis rerum quo harum in ea, sequi adipisci quas possimus neque incidunt cum cupiditate, perspiciatis dignissimos blanditiis quasi eius? Amet sed numquam ratione totam aliquid nihil? Deleniti laboriosam accusantium placeat voluptate 
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
        </Row>

    )
};