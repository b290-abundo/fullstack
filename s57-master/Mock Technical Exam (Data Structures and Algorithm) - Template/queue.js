let collection = [];

    function print(){
        return collection
    }

    // console.log("The printed value is:", print());

    function enqueue(item){
        collection[collection.length] = item;
        return collection
    }

    // console.log(enqueue("John"))
    // console.log(enqueue("Jane"))

    function dequeue(){

        for(let i = 0; i < collection.length -1; i++){
            collection[i] = collection[i + 1];
        }
        collection.length--;
        return collection;
    }

    // dequeue()
    // console.log("Queue elements after dequeue:", collection);
    // console.log(enqueue('Bob'))
    // console.log(enqueue('Cherry'))

    function front(){
        return collection[0];
    }

    // console.log(front())

    function size(){
        return collection.length;
    }

    // console.log(size())

    function isEmpty(){
        return collection.length === 0;
    }

    // console.log(isEmpty())


module.exports = {
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};